var jsondata = {
    "title": "newFlow_1",
    "nodes": {
        "1518079595493": {
            "name": "栅格文件",
            "left": 117,
            "top": 145,
            "type": "fa-sign-in",
            "width": 26,
            "height": 26,
            "alt": true
        },
        "1518079613476": {
            "name": "过滤列信息",
            "left": 211,
            "top": 155,
            "type": "fa-scissors",
            "width": 102,
            "height": 24,
            "alt": true
        },
        "1518079627084": {
            "name": "基础数据",
            "left": 684,
            "top": 42,
            "type": "fa-sign-in",
            "width": 26,
            "height": 26,
            "alt": true
        },
        "1518079651436": {
            "name": "根据主键映射",
            "left": 405,
            "top": 143,
            "type": "fa-columns",
            "width": 102,
            "height": 44,
            "alt": true
        },
        "1518079675972": {
            "name": "根据主键关联",
            "left": 643,
            "top": 147,
            "type": "fa-copy",
            "width": 102,
            "height": 44,
            "alt": true
        },
        "1518079787195": {
            "name": "输出文件",
            "left": 685,
            "top": 293,
            "type": "fa-sign-out",
            "width": 26,
            "height": 26,
            "alt": true
        }
    },
    "lines": {
        "1518079704635": {"type": "sl", "from": "1518079595493", "to": "1518079613476", "name": "", "alt": true},
        "1518079708659": {"type": "sl", "from": "1518079613476", "to": "1518079651436", "name": "", "alt": true},
        "1518079710475": {"type": "sl", "from": "1518079651436", "to": "1518079675972", "name": "", "alt": true},
        "1518079712899": {"type": "sl", "from": "1518079627084", "to": "1518079675972", "name": "", "alt": true},
        "1518079800011": {"type": "sl", "from": "1518079675972", "to": "1518079787195", "name": "", "alt": true}
    },
    "areas": {},
    "initNum": 14
};